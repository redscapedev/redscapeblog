---
title: On fait quoi, on y va ?
date: 2023-07-09
author: Redscape
thumbnail: /img/climat.jpg
tags: ["Climat"]
---

> Note: '*' désigne un paywall derrière le lien

Alors, on y va ?
Ou continue à aller droit dans le mur (et collectivement) ?

Cela faisait longtemps que j'avais envie d'écrire sur le climat. Parce que tout ce qui se passe à ce sujet m'effraie, m'attriste et m'angoisse profondément.
Je me souviens des paroles d'un ami en 2015, me disant qu'il fallait arrêter de s'angoisser pour ça. J'aimerais le rappeler pour lui dire.
Lui dire l'urgence. Lui dire à quel point j'avais raison d'être aussi angoissé.

*Je n'aime pas avoir raison pour ces sujets-là.*

Je crois que ce week-end, c'est la goutte d'eau qui a fait déborder mon vase intérieur. J'ai vu une chaîne de télévision française nommé un présentateur météo (autoproclamé scientifique de surcroît, je ne savais pas que pour vendre des brosses à dents électriques, il fallait un diplôme d'ingénieur) qui a des liens avec l'agroproductivisme. [Quand il faut reprendre des animateurs-vedettes *](https://www.arretsurimages.net/chroniques/sur-le-grill/la-terre-brule-mac-lesggy-encourage-linaction) (en réalité des gens lambdas installés depuis les années 90 sur de grandes chaînes nationales) sur leurs biais cognitifs, résolument, il y a (beaucoup) de travail et du boulot en perspective.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Bonjour <a href="https://twitter.com/MacLesggy?ref_src=twsrc%5Etfw">@MacLesggy</a>, je suis à votre disposition si vous souhaitez des éléments concernant ce qui va déterminer le réchauffement climatique à venir, d&#39;un point de vue géophysique.<br><br>Quelques exemples :</p>&mdash; Dr Valérie Masson-Delmotte (@valmasdel) <a href="https://twitter.com/valmasdel/status/1676942274637230080?ref_src=twsrc%5Etfw">July 6, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Et c'est rien de le dire que ça pose problème. Le discours médiatique servi une fois l'été arrivée, c'est le soleil, le bonheur, être bien entre famille et amis, l'illusion que tu as bossé comme un con tout au long de l'année pour t'offrir cette parenthèse. Mais bien sûr, alors que je viens d'entendre un animateur radio constaté une augmebtation de 22% des réservations en Bretagne contre une baisse en Côte d'Azur, impossible pour moi de ne pas bondir.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">S&#39;il n&#39;y a qu&#39;une seule chose que les médias devraient diffuser aujourd&#39;hui : La Terre bat quotidiennement son record de température ABSOLUE (tous mois confondus) avec une marge statistiquement incroyable.<br>17.18°C, battant les 17,01 d&#39;hier. Il faut changer l&#39;échelle. Réagissons. <a href="https://t.co/fHfYeIEN4K">pic.twitter.com/fHfYeIEN4K</a></p>&mdash; Dr. Serge Zaka (Dr. Zarge) (@SergeZaka) <a href="https://twitter.com/SergeZaka/status/1676465221089599489?ref_src=twsrc%5Etfw">July 5, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Attendez, ça n'est pas fini...

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">17.23°C<br>Pour le 4ème jour de suite, la température mondiale bat son record absolu (depuis 1940, au moins) avec une échappée spectaculaire nettement visible sur le graphique.<br>Ces records mondiaux sont dorénavant doublement confirmés par les outils indépendants 🇺🇸 &amp; 🇪🇺.<a href="https://twitter.com/hashtag/GIEC?src=hash&amp;ref_src=twsrc%5Etfw">#GIEC</a> <a href="https://t.co/arlDM49sRX">pic.twitter.com/arlDM49sRX</a></p>&mdash; Dr. Serge Zaka (Dr. Zarge) (@SergeZaka) <a href="https://twitter.com/SergeZaka/status/1677227806080548865?ref_src=twsrc%5Etfw">July 7, 2023</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Carrément un décrochage statistique, rien que ça !

Oui parce qu'à l'image de ce tweet du [Dr Serge Zaka](https://twitter.com/SergeZaka), c'est la peur qui doit tous nous saisir.

A l'échelle humaine, voir une proportion statistique augmenter de manière significative (on ne prends pas quelques températures par-ci par-là, on en prends un gros tas), il y a de quoi être secoué par cette augmentation (et qui semble durer depuis quelques jours), sans que l'on comprenne bien pourquoi la Terre suffoque à ce point, sauf à se dire que tout ceci n'est qu'une conséquence du réchauffement cliamtique.

> Petit bonus pour ceux qui sont abonnés [à Mediapart, avec une infographie interactive*](https://www.mediapart.fr/journal/ecologie/070723/   climat-la-temperature-journaliere-mondiale-bat-de-nouveaux-records))

> Et pour les devs et autres mathématiciens en herbe, les données composant ces graphiques sont tirés de datas relevés par [Copernicus](https://www.copernicus.eu/fr), un programme européen d'observation de la Terre, la page avec les APIs et d'autres outils se retrouvent [ici](https://cds.climate.copernicus.eu/cdsapp#!/home)).

Alors, voir un journal de 20h s'inquiéter de ce que l'on mets dans un garage (si si, vrai sujet du [JT de TF1 du Vendredi 07 Juillet](https://www.tf1.fr/player/61622361-d7af-4c67-9262-054d6e878ed9?startAt=1929)) plutôt que d'aller constater et surtout alerter (ça n'est pas ça un JT ?), qu'il ne faudrait plus s'étonner d'avoir des politiciens amorphes et sans aucun désir que l'on s'en sortent tous, alliés à une population qui n'en peut plus des catastrophes, provoqué justement par des politiques du vide absolu.

J'imagine déjà des cohortes de voitures migrants du sud de la France vers le Nord, dont certains mourraient en chemin. Oui, si la vision d'apocalypse peut convenir pour un scénario d'un film lambda sur Netflix, sachez qu'il ne faudra que quelques 10 à 20 ans pour qu'il devienne réalité.

Où les régions, fréquemment moquées (les chiottes de la France pour désginer la Bretagne dans les années 90) serviront de refuge à des migrants climatiques, avec un bon gros parallèle à faire sur tous ces gens (dans le sud-est notamment) qui votent allégrement pour des partis refusant actuellement de porter secours aux migrants et pire, ne faisant rien pour stopper ce récahuffement. Iels seront les migrants de demain, et on pourra toujours rigoler de leur imbécilité crasse d'antan.

Mais il faudra les aider, tous. Et puis ceux qui meurent vraiment, aujourd'hui des conséquences de nos actes, nous Occidentaux. Ceux dont la sécheresse provoquée par la raréfaction des pluies, obligent à se déplacer, et doivent modifier leurs modes de vies qui pourtant, étaient loin de polluer autant, comme à l'image de [ce reportage de Mediapart au Kenya en Août 2022*](https://www.mediapart.fr/journal/international/080822/la-crise-climatique-attise-les-tensions-au-kenya).

Non, ça n'est pas ici que vous apprendrez à être un parfait citoyen engagé. Je suis un de ces citoyens. Mais s'informer, et bien s'informer, auprès de médias qui vous relatent les faits, voilà ce qui me semble être un premier pas vers l'acceptation et l'engagement. On ne pourra évoluer que collectivement, et ensemble contre la fin du capitalisme à outrance.

Je réponds à ma propre question, alors on y va ? Non, pas vers ce futur.

J'ai envie de sauver la planète, sauver mon espèce, et sauver mon humanité.

**Redscape**
